.. _license:

License
=======

.. important::

    Plase see the individual package documentation for detailed licensing information.

In general, all packages that are part of the **signac** framework are licensed under the **BSD-3-Clause License**:

.. literalinclude:: licenses/BSD_3_clause.txt

Most source code shown as part of the documentation is released into the public domain:

.. literalinclude:: licenses/public_domain.txt
